name := "juniqe-maze"
version := "0.1"

scalaVersion := "2.13.8"

scalacOptions ++= Seq("-deprecation", "-feature")

val AllTest = "test,it"
val AkkaVersion = "2.6.14"
val AkkaHttpVersion = "10.2.6"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,

  "com.typesafe.akka" %% "akka-testkit" % AkkaVersion % AllTest,
  "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion % AllTest,
  )

configs(IntegrationTest)
Defaults.itSettings

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.9" % AllTest
libraryDependencies += "org.scalatestplus" %% "scalacheck-1-15" % "3.2.9.0" % AllTest
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.32" % AllTest

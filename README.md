## Follower Maze

### The Challenge
Requested was a system which acts as a socket
server, reading events from an *single event source* and forwarding them when
appropriate to *multiple user clients*. See the [original specification](Challenge.md) for details.

### Implementaion

The implementation is based on _Akka Streams_ and is largely inspired by the corresponding part of 
the ["Programming Reactive Systems"](https://www.coursera.org/learn/scala-akka-reactive/) Coursera class. 
Simplified version of the similar task was given as the final home assignment there. 

The core idea is a dynamic message bus, composed of [Merge- and BroadcastHub](https://doc.akka.io/docs/akka/current/stream/stream-dynamic.html#dynamic-fan-in-and-fan-out-with-mergehub-broadcasthub-and-partitionhub)
```scala
  lazy val (merge, broadcast) = MergeHub.source[ByteString].via(incomingParser)
                                        .toMat(BroadcastHub.sink)(Keep.both).run()

```
The processing roughly looks like:
 - _event source(publisher)_ TCP socket connected to `merge` via framing flow. In theory, this allows multiple
   publishers - could be nice extension, but protocol specifies global sequencing which is hardly compatible.
 - all the processing, independent of concrete client, happens in singular `incommingParser` flow. 
   It's components can maintain global state, needed to reintroduce ordering and maintain the followers relations
 - `broadcast` side is routed via client specific flow, which does the filtering for concrete client, 
   and then to client's TCP socket

#### Components
TBD

### Limitaions and shortcomings
TBD

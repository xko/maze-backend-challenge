package juniqe.maze

import akka.event.Logging
import akka.stream.ActorAttributes
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.testkit.TestSubscriber
import akka.stream.testkit.scaladsl.TestSink
import akka.util.ByteString
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.time.SpanSugar._

import scala.util.Random

class ServerWhiteBoxSpec extends AnyWordSpec with TestCommons {
  "follower server" when  {
    "processing events" should {
      import Event._

      "parse events" in {
        val data = List( ByteString("777|F|6"), ByteString("0|50\n\r"), ByteString("1|U"),
                         ByteString("|12|9\r\n"), ByteString("54232|B  \n43|P|32|"), ByteString("56\n"),
                         ByteString("634|S|32\n") )
        val server = new FollowerServer()
        val got = Source(data).via(server.framer).via(server.rawParser).runWith(Sink.seq).futureValue
        got shouldEqual Seq( Follow(777, 60, 50), Unfollow(1, 12, 9),
                             Broadcast(54232), PrivateMsg(43, 32, 56),
                             StatusUpdate(634, 32) )
      }

      "reorder events " in {
        val in = Random.shuffle(List.tabulate(100)(n => Follow(n + 1, 1, 1)))
        val server = new FollowerServer()
        val sorted = Source(in) via server.orderer
        val got = sorted.runWith(Sink.seq).futureValue
        got shouldEqual in.sortBy(_.sequenceNr)
      }

      "not change already sorted" in {
        val in = List.tabulate(100)(n => Follow(n + 1, 1, 1))
        val server = new FollowerServer()
        val got = Source(in).via(server.orderer).runWith(Sink.seq).futureValue
        got shouldEqual in.sortBy(_.sequenceNr)
      }

      "track followers" in {
        val in = List(Follow(1,43,19),Follow(2,25,22),Follow(3,25,533),Unfollow(4,13,12),Follow(5,345,8767),
                      Unfollow(6,25,533),Unfollow(7,43,19),Follow(8,13,12))
        val server = new FollowerServer()
        val (_,endState) = Source(in).via(server.followerStateKeeper).runWith(Sink.last).futureValue
        Followers.isFollowing(endState,43,19) shouldBe false
        Followers.isFollowing(endState,25,22) shouldBe true
        Followers.isFollowing(endState,25,533) shouldBe false
        Followers.isFollowing(endState,13,12) shouldBe true
        Followers.isFollowing(endState,345,8767) shouldBe true
        Followers.isFollowing(endState,99999,88888) shouldBe false
      }
    }
  }

  "handling clients" should {
    import Event._

    "deliver status only to the follower" in {
      val server = new FollowerServer()
      val client1 = connectClient(1, server)
      val client2 = connectClient(2, server)
      val client3 = connectClient(3, server)
      val eventsProbe = connectEvents(server)( StatusUpdate(2, 2), Follow(1, 1, 2) )

      client1.expectNext(StatusUpdate(2, 2))
      client1.expectNoMessage(50.millis)
      client2.expectNext(Follow(1, 1, 2))
      client2.expectNoMessage(50.millis)
      client3.expectNoMessage(50.millis)
      eventsProbe.ensureSubscription().request(1).expectComplete()
    }

    "deliver Follow event ot the one followed" in {
      val server = new FollowerServer()
      val follower = connectClient(1, server)
      val followee = connectClient(2, server)
      val eventsProbe = connectEvents(server)(Follow(1, 1, 2), StatusUpdate(2, 2), StatusUpdate(3, 1))

      followee.expectNext(Follow(1, 1, 2))
      followee.expectNoMessage(50.millis)
      follower.expectNext(StatusUpdate(2, 2))
      follower.expectNoMessage(50.millis)
      eventsProbe.ensureSubscription().request(1).expectComplete()
    }

    "not deliver status before Follow is issued" in {
      val server = new FollowerServer()
      val follower = connectClient(1, server)
      val followee = connectClient(2, server)
      val eventsProbe = connectEvents(server)(StatusUpdate(1, 1), StatusUpdate(2, 2), Follow(3, 1, 2))

      followee.expectNext(Follow(3, 1, 2))
      followee.expectNoMessage(50.millis)
      follower.expectNoMessage(50.millis)
      eventsProbe.ensureSubscription().request(1).expectComplete()
    }


    "deliver private message" in {
      val server = new FollowerServer()
      val sender = connectClient(1, server)
      val receiver = connectClient(2, server)
      val bystander = connectClient(3, server)
      val eventsProbe = connectEvents(server)(Follow(1, 1, 3), PrivateMsg(2, 1, 2))
      // ---- setup done ----

      receiver.expectNext(PrivateMsg(2, 1, 2))
      receiver.expectNoMessage(50.millis)

      sender.expectNoMessage(50.millis)

      bystander.expectNext(Follow(1, 1, 3))
      bystander.expectNoMessage(50.millis)
      eventsProbe.ensureSubscription().request(1).expectComplete()

    }

    def connectClient(id: Int, server: FollowerServer): TestSubscriber.Probe[Event] =
      Source.single(ByteString(s"$id\n"))
        .withAttributes(
          ActorAttributes
            .logLevels(Logging.InfoLevel, Logging.InfoLevel, Logging.InfoLevel)
          )
        .async
        .via(server.clientFlow)
        .via(server.framer).via(server.rawParser)
        .runWith(TestSink.probe).ensureSubscription().request(Int.MaxValue)

    def connectEvents(server: FollowerServer)(events: Event*): TestSubscriber.Probe[Nothing] =
      Source(events.toList).map(_.render ++ server.delimBytes )
        .async
        .via(server.incomingFlow)
        .runWith(TestSink.probe)

  }
}

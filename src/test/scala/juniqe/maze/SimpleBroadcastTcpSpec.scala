package juniqe.maze

import akka.actor.ActorSystem
import akka.stream.scaladsl._
import akka.util.ByteString
import org.scalacheck.Gen
import org.scalatest.time.SpanSugar._
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.{Future, TimeoutException}

class SimpleBroadcastTcpSpec extends AnyWordSpec with TestCommons  {

  "simple broadcast " should {

    "deliver from one to many" in {
      val nMsgs = 10
      val nClients = 15 //TODO: fails with >15 !!
      val server = new SimpleBroadcast()
      val msgGen = Gen.stringOfN( 6, Gen.alphaNumChar.suchThat(!server.delim.contains(_)) )
      val outMsgs = Vector.fill(nMsgs)(msgGen.sample.get)
      val cliConnection = Tcp().outgoingConnection("127.0.0.1", 9099)
      val srcConnection = Tcp().outgoingConnection("127.0.0.1", 9090)

      val cliStream: Source[String, Future[Tcp.OutgoingConnection]] =
        Source.never.viaMat(cliConnection)(Keep.right).via(server.framer).map(_.utf8String).take(nMsgs)
          .completionTimeout(3.seconds).recover { case _:TimeoutException => "!!!" }

      val srcStream = Source(outMsgs).map(ByteString(_) ++ server.delimBytes)
      (for {
        _                       <- server.run()
        (tbConndCls, tbRcvdMsgs) = List.fill(nClients)(cliStream.toMat(Sink.seq)(Keep.both).run()).unzip
        _                       <- Future.sequence(tbConndCls)
        _                        = srcConnection.runWith(srcStream, Sink.ignore)
        msgs                    <- Future.sequence(tbRcvdMsgs)
      } yield {
        msgs
      }).futureValue shouldEqual List.fill(nClients)(outMsgs)
    }
  }

}

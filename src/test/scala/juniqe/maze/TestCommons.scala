package juniqe.maze

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterEach, Suite}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.time.SpanSugar._


trait TestCommons extends ScalaFutures with Matchers with BeforeAndAfterEach { this:Suite =>
  implicit val defaultPatience = PatienceConfig(timeout = 5.seconds)

  private var sys_ : ActorSystem = ActorSystem("ServerSpec")
  implicit def sys:ActorSystem = sys_
  implicit def ecx = sys.dispatcher

  override def beforeEach(): Unit = {
    sys_ = ActorSystem("ServerSpec")
  }

  override def afterEach(): Unit = {
    TestKit.shutdownActorSystem(sys)
  }

}

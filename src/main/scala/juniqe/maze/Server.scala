package juniqe.maze

import akka.actor.ActorSystem
import akka.stream.scaladsl._
import akka.util.ByteString

import scala.concurrent.{ExecutionContext, Future}

trait Server[Msg,Id] {
  implicit val sys:ActorSystem
  implicit lazy val ex: ExecutionContext = sys.dispatcher

  val maxMsgBytes:Int = 256
  val delim:String = "\n"
  val delimBytes = ByteString(delim)
  val framer: Flow[ByteString, ByteString, _] = Framing.delimiter(delimBytes, maxMsgBytes, allowTruncation = false)

  def incomingParser: Flow[ByteString, Msg, _]
  def clientIdReader: Sink[ByteString,Id]
  def clientWriter(clientId: Id): Flow[Msg,ByteString,_]

  lazy val (merge, broadcast) = MergeHub.source[ByteString].via(incomingParser)
                                        .toMat(BroadcastHub.sink)(Keep.both).run()

  lazy val incomingFlow: Flow[ByteString, Nothing, _] = Flow.fromSinkAndSourceCoupled(framer to merge, Source.never)

  def clientFlow: Flow[ByteString, ByteString, _] = {
    val (clientId, clientReader) = clientIdReader.preMaterialize()
    val writer = clientWriter(clientId) map (_ ++ delimBytes)
    Flow.fromSinkAndSource(framer to clientReader, broadcast via writer)
  }


  def run(sourcePort: Int = 9090, clientPort:Int = 9099, host:String = "0.0.0.0") ={

    val pubServer = Tcp().bind(host, sourcePort)
    val subServer = Tcp().bind(host, clientPort).async

    val pubServerReady: Future[Tcp.ServerBinding] = pubServer.to(Sink.foreach { connection =>
      connection.handleWith(incomingFlow)
    }).run()

    val subServerReady: Future[Tcp.ServerBinding] = subServer.to(Sink.foreach { connection =>
      connection.handleWith(clientFlow)
    }).run()

    pubServerReady.zip(subServerReady)
  }
}

trait ServerWithIntId[Msg] extends Server[Msg,Future[Int]]{
  override val clientIdReader: Sink[ByteString, Future[Int]] =
    Flow[ByteString].map(_.utf8String.trim.toInt) .wireTapMat(Sink.head[Int])(Keep.right)
                                                  .to(Sink.ignore)

  override def clientWriter(tbId: Future[Int]): Flow[Msg, ByteString, _] =
    Flow.futureFlow(tbId.map(clientWriter))

  def clientWriter(id:Int) : Flow[Msg, ByteString, _]
}

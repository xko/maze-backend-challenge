package juniqe.maze

import akka.util.ByteString

sealed trait Event {
  /** Sequence number of the message. Starts at 1. */
  def sequenceNr: Int

  /** Payload of the message */
  def render: ByteString
}

object Event {

  def parse(message: String): Event = {
    val fields = message.split("\\|")
    val seqNr = fields.head.toInt

    fields(1) match {
      case "F" => Event.Follow(seqNr, fields(2).toInt, fields(3).toInt)
      case "U" => Event.Unfollow(seqNr, fields(2).toInt, fields(3).toInt)
      case "B" => Event.Broadcast(seqNr)
      case "P" => Event.PrivateMsg(seqNr, fields(2).toInt, fields(3).toInt)
      case "S" => Event.StatusUpdate(seqNr, fields(2).toInt)
    }
  }

  /**
   * Follow: Only the To User Id should be notified
   */
  final case class Follow( sequenceNr: Int, fromUserId: Int, toUserId: Int ) extends Event {
    def render: ByteString = ByteString(s"$sequenceNr|F|$fromUserId|$toUserId")
  }

  /**
   * Unfollow: No clients should be notified
   */
  final case class Unfollow( sequenceNr: Int, fromUserId: Int, toUserId: Int ) extends Event {
    def render: ByteString = ByteString(s"$sequenceNr|U|$fromUserId|$toUserId")
  }
  /**
   * Broadcast: All connected user clients should be notified
   */
  final case class Broadcast(sequenceNr: Int ) extends Event {
    def render: ByteString = ByteString(s"$sequenceNr|B")
  }
  /**
   * Private Message: Only the To User Id should be notified
   */
  final case class PrivateMsg( sequenceNr: Int, fromUserId: Int, toUserId: Int ) extends Event {
    def render: ByteString = ByteString(s"$sequenceNr|P|$fromUserId|$toUserId")
  }

  /**
   * Status Update: All current followers of the From User ID should be notified
   */
  final case class StatusUpdate( sequenceNr: Int, fromUserId: Int ) extends Event {
    def render: ByteString = ByteString(s"$sequenceNr|S|$fromUserId")
  }
}

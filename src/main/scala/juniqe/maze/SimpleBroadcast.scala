package juniqe.maze

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.util.ByteString

class SimpleBroadcast(implicit val sys: ActorSystem) extends Server[ByteString, Any] {
  override val incomingParser: Flow[ByteString, ByteString, _] = Flow[ByteString]
  override val clientIdReader: Sink[ByteString, _] = Sink.ignore

  override def clientWriter(clientId: Any): Flow[ByteString, ByteString, _] = Flow[ByteString]
}

class SimpleBroadcastWithId(implicit val sys: ActorSystem ) extends ServerWithIntId[ByteString] {
  override val incomingParser: Flow[ByteString, ByteString, _] = Flow[ByteString]

  override def clientWriter(id: Int): Flow[ByteString, ByteString, _] =
    Flow[ByteString].map(ByteString(s"$id:").concat(_))

}

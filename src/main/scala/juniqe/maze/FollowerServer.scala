package juniqe.maze

import akka.actor.ActorSystem
import akka.stream.scaladsl.Flow
import akka.util.ByteString

import scala.collection.mutable


class FollowerServer(implicit val sys: ActorSystem ) extends ServerWithIntId[(Event,Followers.State)] {
  override def incomingParser: Flow[ByteString, (Event,Followers.State), _] =
     rawParser via orderer via followerStateKeeper

  val rawParser: Flow[ByteString, Event, _] = Flow[ByteString].map(_.utf8String.trim).map(Event.parse)

  val orderer: Flow[Event, Event, _] = Flow[Event].statefulMapConcat { () =>
    var expected = 1
    val buffer = mutable.PriorityQueue.empty(Ordering.by[Event, Int](_.sequenceNr).reverse)
    def handle(event: Event): List[Event] = {
      if (event.sequenceNr == expected) {
        expected = expected + 1
        event :: (if (buffer.isEmpty) Nil else handle(buffer.dequeue()))
      } else {
        buffer.enqueue(event)
        Nil
      }
    }
    handle
  }

  val followerStateKeeper: Flow[Event,(Event,Followers.State),_] = Flow[Event].statefulMapConcat { () =>
    val followers = new Followers

    { case ev@Event.Follow(_, fromUserId, toUserId) =>
        (ev, followers.follow(fromUserId,toUserId)) :: Nil
      case ev@Event.Unfollow(_, fromUserId, toUserId) =>
        (ev, followers.unfollow(fromUserId,toUserId)) :: Nil
      case ev => (ev, followers.state) :: Nil
    }
  }

  override def clientWriter(id: Int): Flow[(Event,Followers.State), ByteString, _] = {
    Flow[(Event,Followers.State)].filter{
      case (Event.StatusUpdate(_,fromUserId), state) => Followers.isFollowing(state, id, fromUserId)
      case (Event.PrivateMsg(_,_,toUserId),_)        => toUserId == id
      case (Event.Follow(_,_,toUserId),_)            => toUserId == id
      case (Event.Broadcast(_),_)                    => true
      case _                                         => false
    } map (_._1.render)
  }
}

object Followers {
  type State = Map[Int, Set[Int]]
  def isFollowing(state: State, who:Int, whom: Int) = state.get(who).exists(_.contains(whom))
}

class Followers {
  import Followers.State

  var state: State = Map.empty

  def update(key: Int)(upd: Set[Int] => Set[Int]): State = {
    state = state.updatedWith(key)(_.orElse(Some(Set.empty[Int])).map(upd))
    state
  }

  def follow(who: Int, whom: Int): State = update(who)(_+whom)
  def unfollow(who: Int, whom: Int): State = update(who)(_-whom)
}

object FollowerServer {
  def main(args: Array[String]): Unit = {
    implicit val sys = ActorSystem("juniqe-maze")
    import sys.dispatcher
    new FollowerServer().run().recover(_ => sys.terminate())
  }
}
